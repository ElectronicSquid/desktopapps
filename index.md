---
title: "Mon environnement numérique pour \"Desktop\" :)"
order: 0
in_menu: true
---
### > Communiquer

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Element.io%20(ex%20Riot.im).png">
  </div>
  <div>
    <h2>Element (ex Riot)</h2>
    <p>Element rassemble toutes vos conversations et intégrations applicatives en une seule application.</p>
    <div>
      <a href="https://framalibre.org/notices/element-ex-riot.html">Vers la notice Framalibre</a>
      <a href="https://element.io">Vers le site</a>
    </div>
  </div>
</article>

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Evolution.png">
  </div>
  <div>
    <h2>Evolution</h2>
    <p>Evolution : Le client de messagerie qui n'a rien a envier aux autres.</p>
    <div>
      <a href="https://framalibre.org/notices/evolution.html">Vers la notice Framalibre</a>
      <a href="https://wiki.gnome.org/Apps/Evolution/">Vers le site</a>
    </div>
  </div>
</article>

### > Écouter de la Musique

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Quod%20Libet.png">
  </div>
  <div>
    <h2>Quod Libet</h2>
    <p>Un lecteur musical bit-perfect à la fois simple et puissant, pour amateur éclairé ou débutant.</p>
    <div>
      <a href="https://framalibre.org/notices/quod-libet.html">Vers la notice Framalibre</a>
      <a href="https://quodlibet.readthedocs.io/">Vers le site</a>
    </div>
  </div>
</article>

### > Écrire

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/LibreOffice.png">
  </div>
  <div>
    <h2>LibreOffice</h2>
    <p>Une suite bureautique issue d'OpenOffice.org.</p>
    <div>
      <a href="https://framalibre.org/notices/libreoffice.html">Vers la notice Framalibre</a>
      <a href="https://fr.libreoffice.org/">Vers le site</a>
    </div>
  </div>
</article>

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Zettlr.png">
  </div>
  <div>
    <h2>Zettlr</h2>
    <p>Zettlr a été créé par un chercheur pour gérer et organiser la prise de notes, la rédaction et l’édition.</p>
    <div>
      <a href="https://framalibre.org/notices/zettlr.html">Vers la notice Framalibre</a>
      <a href="https://www.zettlr.com/">Vers le site</a>
    </div>
  </div>
</article>

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/ZIM.png">
  </div>
  <div>
    <h2>ZIM</h2>
    <p>Un wiki personnel sur votre bureau. Écrivez et organisez des petits textes, images et fichiers.</p>
    <div>
      <a href="https://framalibre.org/notices/zim.html">Vers la notice Framalibre</a>
      <a href="http://zim-wiki.org/">Vers le site</a>
    </div>
  </div>
</article>

### > Rechercher

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/searx.png">
  </div>
  <div>
    <h2>searx</h2>
    <p>Un méta-moteur de recherche respectueux de votre vie privée !</p>
    <div>
      <a href="https://framalibre.org/notices/searx.html">Vers la notice Framalibre</a>
      <a href="https://searx.github.io/searx/">Vers le site</a>
    </div>
  </div>
</article>

### > Stocker sur le Net

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Gitlab.png">
  </div>
  <div>
    <h2>Gitlab</h2>
    <p>Une forge logicielle extrêmement complète&nbsp;: forks, merge requests, tickets, intégration continue, communication avec d'autres services comme Mattermost…</p>
    <div>
      <a href="https://framalibre.org/notices/gitlab.html">Vers la notice Framalibre</a>
      <a href="https://gitlab.com">Vers le site</a>
    </div>
  </div>
</article>

<article class="framalibre-notice">
  <div>
    <img src="https://framalibre.org/images/logo/Nextcloud.png">
  </div>
  <div>
    <h2>Nextcloud</h2>
    <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
    <div>
      <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
      <a href="https://nextcloud.com/">Vers le site</a>
    </div>
  </div>
</article> 